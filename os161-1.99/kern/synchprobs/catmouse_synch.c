#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>

/* 
 * This simple default synchronization mechanism allows only creature at a time to
 * eat.   The globalCatMouseSem is used as a a lock.   We use a semaphore
 * rather than a lock so that this code will work even before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
//static struct semaphore *globalCatMouseSem;
struct semaphore *full;
struct semaphore *empty;
struct cv *mousedoneeating;
struct cv *catdoneeating;
struct lock *mutex;
struct lock **bowlmutices;
volatile int catsout = 0;
volatile int miceout = 0;
volatile int bowls = 0;
  


/* 
 * The CatMouse simulation will call this function once before any cat or
 * mouse tries to each.
 *
 * You can use it to initialize synchronization and other variables.
 * 
 * parameters: the number of bowls
 */
void
catmouse_sync_init(int bowls)
{
  /* replace this default implementation with your own implementation of catmouse_sync_init */

  (void)bowls; /* keep the compiler from complaining about unused parameters */
  full = sem_create("usedbowls", 0);
  if (full == NULL) {
    panic("could not create global full sem");
  }
  empty = sem_create("emptybowls", 1);
  if (empty == NULL) {
    panic("could not create global empty sem");
  }
  mousedoneeating = cv_create("mousedoneeating");
  if (mousedoneeating == NULL) {
    panic("could not create global mousedoneeating cv");
  }
  catdoneeating = cv_create("catdoneeating");
  if (catdoneeating == NULL) {
    panic("could not create global catdoneeating cv");
  }
  mutex = lock_create("mutex");
  if (mutex == NULL) {
    panic("could not create global lock mutex");
  }
  bowlmutices = kmalloc(bowls * sizeof(struct lock *)); 
  if (bowlmutices == NULL) {
    panic("could not create global locks bowlmutices");
  }
  for (int i = 0; i < bowls; i++) {
    bowlmutices[i] = lock_create("bowlmutex");
  }
  return;
}

/* 
 * The CatMouse simulation will call this function once after all cat
 * and mouse simulations are finished.
 *
 * You can use it to clean up any synchronization and other variables.
 *
 * parameters: the number of bowls
 */
void
catmouse_sync_cleanup(int bowls)
{
  /* replace this default implementation with your own implementation of catmouse_sync_cleanup */
  (void)bowls; /* keep the compiler from complaining about unused parameters */
  //KASSERT(globalCatMouseSem != NULL);
  //sem_destroy(globalCatMouseSem);
  //KASSERT(!lock_do_i_hold(mutex));
  KASSERT(mutex != NULL);
  KASSERT(catdoneeating != NULL);
  KASSERT(mousedoneeating != NULL);
  KASSERT(empty != NULL);
  KASSERT(full != NULL);
  KASSERT(bowlmutices != NULL);
  for (int i = 0; i < bowls; i++) {
    lock_destroy(bowlmutices[i]);
  }
  lock_destroy(mutex);
  cv_destroy(mousedoneeating);
  cv_destroy(catdoneeating);
  sem_destroy(empty);
  sem_destroy(full);
}


/*
 * The CatMouse simulation will call this function each time a cat wants
 * to eat, before it eats.
 * This function should cause the calling thread (a cat simulation thread)
 * to block until it is OK for a cat to eat at the specified bowl.
 *
 * parameter: the number of the bowl at which the cat is trying to eat
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
cat_before_eating(unsigned int bowl) 
{
  /* replace this default implementation with your own implementation of cat_before_eating */
  (void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);
  //P(empty);
  lock_acquire(mutex);
  while(miceout != 0) { //wait for a mouse to finish eating and check if mice are gone
    DEBUG(DB_THREADS,"cat waiting at bowl %d \n", bowl);
    cv_wait(mousedoneeating, mutex);
    DEBUG(DB_THREADS,"cat done waiting at bowl %d with %d mice left\n", bowl, miceout);
  }
    DEBUG(DB_THREADS,"cat eating at bowl %d \n", bowl);
    catsout++;
  //V(empty);
  lock_release(mutex);
  lock_acquire(bowlmutices[bowl-1]);
  //lock_release(mutex);
}

/*
 * The CatMouse simulation will call this function each time a cat finishes
 * eating.
 *
 * You can use this function to wake up other creatures that may have been
 * waiting to eat until this cat finished.
 *
 * parameter: the number of the bowl at which the cat is finishing eating.
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
cat_after_eating(unsigned int bowl) 
{
  /* replace this default implementation with your own implementation of cat_after_eating */
  (void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);
  //V(globalCatMouseSem);
  lock_acquire(mutex);
    DEBUG(DB_THREADS,"cat after eating at bowl %d \n", bowl);
  //lock_acquire(mutex);
    catsout--;
  //lock_release(mutex);
    DEBUG(DB_THREADS,"cat done eating at bowl %d \n", bowl);
    cv_broadcast(catdoneeating, mutex);
  lock_release(bowlmutices[bowl-1]);
  lock_release(mutex);
}

/*
 * The CatMouse simulation will call this function each time a mouse wants
 * to eat, before it eats.
 * This function should cause the calling thread (a mouse simulation thread)
 * to block until it is OK for a mouse to eat at the specified bowl.
 *
 * parameter: the number of the bowl at which the mouse is trying to eat
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
mouse_before_eating(unsigned int bowl) 
{
  /* replace this default implementation with your own implementation of mouse_before_eating */
  (void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //P(empty);
  lock_acquire(mutex);
  while(catsout != 0) { //wait for a mouse to finish eating and check if mice are gone
    DEBUG(DB_THREADS,"mouse waiting at bowl %d \n", bowl);
    cv_wait(catdoneeating, mutex);
    DEBUG(DB_THREADS,"mouse done waiting at bowl %d with %d cats left\n", bowl, catsout);
  }
  //lock_acquire(mutex);
    DEBUG(DB_THREADS,"mouse eating at bowl %d \n", bowl);
    miceout++;
  lock_release(mutex);
  lock_acquire(bowlmutices[bowl-1]);
  //V(empty);
  //lock_release(mutex);
}

/*
 * The CatMouse simulation will call this function each time a mouse finishes
 * eating.
 *
 * You can use this function to wake up other creatures that may have been
 * waiting to eat until this mouse finished.
 *
 * parameter: the number of the bowl at which the mouse is finishing eating.
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
mouse_after_eating(unsigned int bowl) 
{
  /* replace this default implementation with your own implementation of mouse_after_eating */
  (void)bowl;  /* keep the compiler from complaining about an unused parameter */
  lock_acquire(mutex);
    DEBUG(DB_THREADS,"mouse after eating at bowl %d \n", bowl);
  //lock_acquire(mutex);
    miceout--;
  //lock_release(mutex);
    DEBUG(DB_THREADS,"mouse done eating at bowl %d \n", bowl);
    cv_broadcast(mousedoneeating, mutex);
  lock_release(bowlmutices[bowl-1]);
  lock_release(mutex);
}
