#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <spl.h>
#include <synch.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include <mips/trapframe.h>
#include <vfs.h>
#include <vm.h>
#include "opt-A2.h"
#include <limits.h>

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  #if OPT_A2
  #else
  (void)exitcode;
  #endif
  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  #if OPT_A2
  int id = curthread->pid;
  struct pid *temp = find_pid(id);
  lock_acquire(temp->pid_lock);
  temp->exitcode = _MKWVAL(exitcode);
  temp->exited = 1;
  cv_signal(temp->pid_cv, temp->pid_lock);
  lock_release(temp->pid_lock);
  #endif
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);
  int spl = splhigh();
  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */

  proc_destroy(p);
  splx(spl);
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
#if OPT_A2
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = curthread->pid;
  return(0);
}
#else
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = 1;
  return(0);
}
#endif

/* stub handler for waitpid() system call                */
#if OPT_A2
int
sys_waitpid(pid_t pid,
      userptr_t status,
      int options,
      pid_t *retval)
{
  int exitstatus;
  int result;
  *retval = pid;
  if (status == NULL || ((int) status) % 4 != 0) {
    return (EFAULT);
  }

  if (options != 0) {
    return (EINVAL);
  }

  struct pid *child = NULL;
  child = find_pid(pid);
  if (child == NULL) {
    return (ESRCH);
  }

  if (child->parentpid != curthread->pid) {
    return (ECHILD);
  }
  lock_acquire(child->pid_lock);
  while(child->exited == 0) { //wait untill child finished/exit
    cv_wait(child->pid_cv,child->pid_lock);
  }
  exitstatus = (int)(child->exitcode);
  pid_change_status(pid, PID_PQUIT);
  lock_release(child->pid_lock);
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  (void)options;
  return 0;
}
#else
int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}
#endif
#if OPT_A2
int
sys_fork(struct trapframe *tf) {
  struct proc *child_proc = proc_create_runprogram(curthread->t_name);
  if (child_proc == NULL) {
    return ENOMEM;
  }
  void (*child)(void *, unsigned long) = (void *)enter_forked_process;

  unsigned long a0 = tf->tf_a0;

  struct trapframe *child_tf = kmalloc(sizeof(struct trapframe));
  if (child_tf == NULL) {
    return ENOMEM;
  }
  memcpy(child_tf, tf, sizeof(struct trapframe));
  
  int err = as_copy(curproc_getas(), &child_proc->p_addrspace);
  if (err) {
    return ENOMEM;
  }
  int fork_result = thread_fork(child_proc->p_name, child_proc, child, child_tf, a0);
  if (fork_result != 0) {
        proc_destroy(child_proc);
        //ERROR
        return ENOMEM;
  }
  struct pid *child_pid = find_pid(threadarray_get(&child_proc->p_threads,0)->pid);
  child_pid->parentpid = curthread->pid;
  return child_pid->mypid;
}

int
sys_execv(const char *program, char ** args) {
  (void) program;
  (void) args;

  struct addrspace *as;
  struct vnode *v;
  vaddr_t entrypoint, stackptr;
  int result;
  size_t argsize = 0;
  //int stackAlignConstraint = 8;

  KASSERT(curproc != NULL);

  int nargs = 0;
  int i = 0;
  while (args[i] != NULL) {
      nargs++;
      i++;
  }

  //int offsets[nargs];

  if (nargs > 16) { //from menu.c
      kprintf("Command line has too many words\n");
      return E2BIG;
  }

  char **kern_args = kmalloc(sizeof (char) * (strlen(program) + 1));
  if (kern_args == NULL) {
    return ENOMEM;
  }

  char* kern_program = kmalloc(sizeof (char) * (strlen(program) + 1));
  copyinstr((const_userptr_t) program, kern_program, (strlen(program) + 1), NULL);
  argsize += strlen(kern_args[0]) + 1;
  for (int i = 1; i < nargs; i++) {
    if (strlen(args[i]) < 1) {
      return EINVAL;
    }
    kern_args[i] = kmalloc(sizeof (char*) * (strlen(args[i]) + 1));
    argsize += strlen(kern_args[i]) + 1;
    copyinstr((const_userptr_t)args[i], kern_args[i], strlen(args[i])+1, NULL);
    //offsets[i] = ARG_MAX - curbuf;
    //stackAlignConstraint += strlen(kern_args[i]) + 5;
    //curbuf = curbuf - arglength;

  }/* Open the file. */
  result = vfs_open(kern_program, O_RDONLY, 0, &v);
  if (result) {
    return result;
  }

  int spl = splhigh();
  //kprintf("DESTROYING: %x\n", (int)curproc_getas());

  //vm_cmprint();
  as_destroy(curproc_getas());
  //vm_cmprint();
  //kprintf("DESTROYED: %x\n", (int)curproc_getas());

  splx(spl);
  //KASSERT(curproc_getas() == NULL);
  //as_destroy(curproc_getas());
  //curproc_setas(NULL);
  /* We should be a new process. */

  /* Create a new address space. */
  as = as_create();
  if (as == NULL) {
    vfs_close(v);
    return ENOMEM;
  }

  /* Switch to it and activate it. */
  spl = splhigh();
  //kprintf("pbase1: %x\n", (int)curproc_getas()->as_pbase1);

  curproc_setas(as);
  as_activate();
  //kprintf("pbase12: %x\n", (int)curproc_getas()->as_pbase1);

  splx(spl);


  /* Load the executable. */
  result = load_elf(v, &entrypoint);
  if (result) {
    /* p_addrspace will go away when curproc is destroyed */
    vfs_close(v);
    return result;
  }
  kfree(kern_program);

  /* Done with the file now. */
  vfs_close(v);

  /* Define the user stack in the address space */
  result = as_define_stack(as, &stackptr);
  if (result) {
    /* p_addrspace will go away when curproc is destroyed */
    return result;
  }

  // copying args to new userspace

  char *argptr = (char*)stackptr - argsize;
  char ** user_args = (char**)ROUNDUP((vaddr_t)argptr, 4) - sizeof(char**) * (nargs + 1);

  for(int i = 0; i < nargs; i++)
  {
    copyoutstr(kern_args[i], (userptr_t)argptr, strlen(kern_args[i]) + 1, NULL);
    user_args[i] = argptr;
    argptr += strlen(kern_args[i]) + 1;
  }
  user_args[nargs] = NULL;
  // free kern_arguments
  for (int i = 0; i < nargs; i++) {
    kfree(kern_args[i]);
  }
  kfree(kern_args);

  //kprintf("%d",(int)user_args);
  //kprintf("%d",(int)user_args);
  /* Warp to user mode. */
  as->mode = 1;
  //kprintf("CM FOR %x\n", (int)as->pagetable);
  //vm_cmprint();
  enter_new_process(nargs /*nargs*/, (userptr_t)user_args /*userspace addr of argv*/,
        (vaddr_t)user_args, entrypoint);
  
  /* enter_new_process does not return. */
  panic("enter_new_process returned\n");
  return EINVAL;
}

#endif
