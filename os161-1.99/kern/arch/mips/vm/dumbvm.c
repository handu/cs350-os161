/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <proc.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <syscall.h>
#include "opt-A3.h"
#include <synch.h>

/*
 * Dumb MIPS-only "VM system" that is intended to only be just barely
 * enough to struggle off the ground. You should replace all of this
 * code while doing the VM assignment. In fact, starting in that
 * assignment, this file is not included in your kernel!
 */

/* under dumbvm, always have 48k of user stack */
#define DUMBVM_STACKPAGES    12

/*
 * Wrap rma_stealmem in a spinlock.
 */
static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;
#if OPT_A3
//struct lock coremap_lock;
struct coremapentry *coremap;
int coremappages;
//void vm_cmprint(void);//local
void vm_cmprint(void) {
	int spl = splhigh();
	for (int i = 0; i <= coremappages; i++) {
		kprintf("Coremap PRINT Index: %d PADDR: 0x%x STATE: %d PTIndex: 0x%x \n", i, i*PAGE_SIZE, coremap[i].page_state, coremap[i].index);
	}
	splx(spl);
	return;
}

#endif
void
vm_bootstrap(void)
{
	/* Do nothing. */
	#if OPT_A3
	//coremap_lock = lock_create("coremap lock");
	//lock_acquire(coremap_lock);

	int spl = splhigh();
		paddr_t fpaddr, lpaddr;
		ram_getsize(&fpaddr, &lpaddr);
		coremappages = (ROUNDUP(lpaddr, PAGE_SIZE)-1) / PAGE_SIZE;
		coremap = (struct coremapentry*)PADDR_TO_KVADDR(fpaddr);
		paddr_t freeaddr = fpaddr + coremappages * sizeof(struct coremapentry);
		paddr_t cpaddr = 0;
		int i;
		for (i = 0; cpaddr < freeaddr; i++) {
			//kprintf("Coremap Init Index: %d PADDR: 0x%x STATE: %d\n", i, cpaddr, 0);
			//coremap[i].as = NULL;
			coremap[i].page_state = 0; // set kernel coremap entries to fixed
			coremap[i].index = 0;
			cpaddr += PAGE_SIZE;
		}
		for (; cpaddr < lpaddr; i++) {
			//kprintf("Coremap Init Index: %d PADDR: 0x%x STATE: %d\n", i, cpaddr, 1);
			//coremap[i].as = NULL;
			coremap[i].page_state = 1; // set kernel coremap entries to free
			coremap[i].index = 0;
			cpaddr += PAGE_SIZE;
		}
		kprintf("We have pages: %d\n", i);

	splx(spl);
	//lock_release(coremap_lock);
	#endif
}
#if OPT_A3
/*static
paddr_t
getpage()
{
	if (coremap == NULL) {
		spinlock_acquire(&stealmem_lock);

		addr = ram_stealmem(1);
		
		spinlock_release(&stealmem_lock);
	} else {

		spinlock_acquire(&stealmem_lock);
		for (int i = 0; i < coremappages && cpages < (int)npages; i++) {
			if (coremap[i].page_state == 1) {
				addr = (i + 1) * PAGE_SIZE;
				coremap[i].page_state = 2;
				break;
			}
		}

		spinlock_release(&stealmem_lock);
	}
	return addr;
}*/
static
paddr_t
getppages(unsigned long npages)
{
	paddr_t addr;

	if (coremap == NULL) {
		spinlock_acquire(&stealmem_lock);

		addr = ram_stealmem(npages);
		
		spinlock_release(&stealmem_lock);
	} else {
		//kprintf("not held");
		spinlock_acquire(&stealmem_lock);
		//kprintf("held");
		int cpages = 0;
		addr = 0;
		for (int i = 0; i < coremappages && cpages < (int)npages; i++) {
			if (coremap[i].page_state == 1) {
				cpages++;
			} else {
				cpages = 0;
				addr = (i + 1) * PAGE_SIZE;
			}
		}

		for (int i = (int)(addr/PAGE_SIZE); i < (int)(addr/PAGE_SIZE + (int)npages); i++) {
			coremap[i].page_state = 2;
			coremap[i].index = (int)addr; // continuous memory
			//kprintf("xx");
		}
		//kprintf("GETPAGE %x\n", (int)addr);

		spinlock_release(&stealmem_lock);
		if (cpages != (int)npages) {
			return 0;
		}

		//kprintf("%x\n",addr);
	}
	return addr;
}
#else
static
paddr_t
getppages(unsigned long npages)
{
	paddr_t addr;

	spinlock_acquire(&stealmem_lock);

	addr = ram_stealmem(npages);
	
	spinlock_release(&stealmem_lock);
	return addr;
}
#endif

/* Allocate/free some kernel-space virtual pages */
#if OPT_A3
vaddr_t 
alloc_kpages(int npages)
{
	if (coremap == NULL) {
		paddr_t pa;
		pa = getppages(npages);
		if (pa==0) {
			return 0;
		}
		return PADDR_TO_KVADDR(pa);
	} else {
		paddr_t pa;
		pa = getppages(npages);
		if (pa==0) {
			return 0;
		}
		return PADDR_TO_KVADDR(pa);
	}
}
#else
vaddr_t 
alloc_kpages(int npages)
{
	paddr_t pa;
	pa = getppages(npages);
	if (pa==0) {
		return 0;
	}
	return PADDR_TO_KVADDR(pa);
}
#endif

void 
free_kpages(vaddr_t addr)
{
	/* nothing - leak the memory. */

	(void)addr;
	#if OPT_A3
	//kprintf("FREEING: %x\n", addr);
	addr = KVADDR_TO_PADDR(addr);
	int spl = splhigh();
	int count = 0;
	for (int i = 0; i < coremappages; i++) {
		if (coremap[i].index == (int)addr) {
			coremap[i].page_state = 1;
			count++;
		}
	}
	splx(spl);
	//kprintf("FREED %d PAGES %x, %x\n", (int)(count), addr, addr/PAGE_SIZE);
	//vm_cmprint();
	#endif
}

void
vm_tlbshootdown_all(void)
{
	panic("dumbvm tried to do tlb shootdown?!\n");
}

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("dumbvm tried to do tlb shootdown?!\n");
}
#if OPT_A3
int
vm_fault(int faulttype, vaddr_t faultaddress)
{
	vaddr_t vbase1, vtop1, vbase2, vtop2, stackbase, stacktop;
	paddr_t paddr, paddr2;
	int i;
	uint32_t ehi, elo;
	struct addrspace *as;
	int spl;

	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype) {
	    case VM_FAULT_READONLY:
		/* We always create pages read-write, so we can't get this */
		//panic("dumbvm: got VM_FAULT_READONLY\n");
		DEBUG(DB_THREADS, "VM_FAULT_READONLY fault: 0x%x\n", faultaddress);
		sys__exit(-1);
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = curproc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* Assert that the address space has been set up properly. */
	if (as->as_pbase1 == 0) {
	//kprintf("VM_FAULT 0x%x\n", (int)as);

	}
	if (!(as->as_vbase1 != 0 && as->as_pbase1 != 0 && as->as_vbase2 != 0 && as->as_pbase2 != 0)) {
		kprintf("VM_ERROR AS NOT SET UP: as = 0x%x\n", (int)as);
		kprintf("VM_ERROR AS NOT SET UP: as->as_vbase1 = 0x%x\n", (int)as->as_vbase1);
		kprintf("VM_ERROR AS NOT SET UP: as->as_vbase2 = 0x%x\n", (int)as->as_vbase2);
		kprintf("VM_ERROR AS NOT SET UP: as->as_pbase1 = 0x%x\n", (int)as->as_pbase1);
		kprintf("VM_ERROR AS NOT SET UP: as->as_pbase2 = 0x%x\n", (int)as->as_pbase2);
		vm_cmprint();
		sys__exit(-1);
	}
	KASSERT(as->as_vbase1 != 0);
	KASSERT(as->as_pbase1 != 0);
	KASSERT(as->as_npages1 != 0);
	KASSERT(as->as_vbase2 != 0);
	KASSERT(as->as_pbase2 != 0);
	KASSERT(as->as_npages2 != 0);
	KASSERT(as->as_stackpbase != 0);
	KASSERT((as->as_vbase1 & PAGE_FRAME) == as->as_vbase1);
	KASSERT((as->as_pbase1 & PAGE_FRAME) == as->as_pbase1);
	KASSERT((as->as_vbase2 & PAGE_FRAME) == as->as_vbase2);
	KASSERT((as->as_pbase2 & PAGE_FRAME) == as->as_pbase2);
	KASSERT((as->as_stackpbase & PAGE_FRAME) == as->as_stackpbase);

	vbase1 = as->as_vbase1;
	vtop1 = vbase1 + as->as_npages1 * PAGE_SIZE;
	vbase2 = as->as_vbase2;
	vtop2 = vbase2 + as->as_npages2 * PAGE_SIZE;
	stackbase = USERSTACK - DUMBVM_STACKPAGES * PAGE_SIZE;
	stacktop = USERSTACK;

	uint32_t elot;
	int pageindex = 0;
	if (faultaddress >= vbase1 && faultaddress < vtop1) {
		paddr = (faultaddress - vbase1) + as->as_pbase1;
		pageindex = (faultaddress - vbase1)/PAGE_SIZE;
		paddr2 = as->pagetable[pageindex].paddr;
		if (as->mode) { // if in usermode
			elot = paddr2 | 0x0 | TLBLO_VALID;
		} else {
			elot = paddr2 | TLBLO_DIRTY | TLBLO_VALID;
		}
		//DEBUG(DB_THREADS, "text: paddrs: %x, %x, from %x, %x, %d\n", paddr, paddr2, faultaddress, vbase1, pageindex);
	}
	else if (faultaddress >= vbase2 && faultaddress < vtop2) {
		paddr = (faultaddress - vbase2) + as->as_pbase2;
		pageindex = as->as_npages1 + (faultaddress - vbase2)/PAGE_SIZE;
		paddr2 = as->pagetable[pageindex].paddr;
		elot = paddr2 | TLBLO_DIRTY | TLBLO_VALID;
		//DEBUG(DB_THREADS, "data: paddrs: %x, %x, from %x, %x, %d\n", paddr, paddr2, faultaddress, vbase2, pageindex);
	}
	else if (faultaddress >= stackbase && faultaddress < stacktop) {
		paddr = (faultaddress - stackbase) + as->as_stackpbase;
		pageindex = as->as_npages1 + as->as_npages2 + (faultaddress - stackbase)/PAGE_SIZE;
		paddr2 = as->pagetable[pageindex].paddr;
		elot = paddr2 | TLBLO_DIRTY | TLBLO_VALID;
		//DEBUG(DB_THREADS, "stack: paddrs: %x, %x, from %x, %x, %d\n", paddr, paddr2, faultaddress, stacktop, pageindex);
	}
	else {
		return EFAULT;
	}
	if (paddr != paddr2) {
		//kprintf("0x%x, 0x%x\n", (int)paddr, (int) paddr2);
		//KASSERT(paddr == paddr2);
	}

	/* make sure it's page-aligned */
	KASSERT((paddr2 & PAGE_FRAME) == paddr2);

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_read(&ehi, &elo, i);
		if (elo & TLBLO_VALID) {
			continue;
		}
		ehi = faultaddress;
		elo = elot;
		DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr2);
		//kprintf("tlb_write");
		tlb_write(ehi, elo, i);
		splx(spl);
		return 0;
	}
	//elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
		//kprintf("tlb_random1");
	tlb_random(faultaddress, elot);
		//kprintf("tlb_random2");
	DEBUG(DB_VM, "tlb_random dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr2);
	//kprintf("dumbvm: Ran out of TLB entries - cannot handle page fault\n");
	splx(spl);
	return 0;
}
#else
int
vm_fault(int faulttype, vaddr_t faultaddress)
{
	vaddr_t vbase1, vtop1, vbase2, vtop2, stackbase, stacktop;
	paddr_t paddr;
	int i;
	uint32_t ehi, elo;
	struct addrspace *as;
	int spl;

	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype) {
	    case VM_FAULT_READONLY:
		/* We always create pages read-write, so we can't get this */
		panic("dumbvm: got VM_FAULT_READONLY\n");
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = curproc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* Assert that the address space has been set up properly. */
	KASSERT(as->as_vbase1 != 0);
	KASSERT(as->as_pbase1 != 0);
	KASSERT(as->as_npages1 != 0);
	KASSERT(as->as_vbase2 != 0);
	KASSERT(as->as_pbase2 != 0);
	KASSERT(as->as_npages2 != 0);
	KASSERT(as->as_stackpbase != 0);
	KASSERT((as->as_vbase1 & PAGE_FRAME) == as->as_vbase1);
	KASSERT((as->as_pbase1 & PAGE_FRAME) == as->as_pbase1);
	KASSERT((as->as_vbase2 & PAGE_FRAME) == as->as_vbase2);
	KASSERT((as->as_pbase2 & PAGE_FRAME) == as->as_pbase2);
	KASSERT((as->as_stackpbase & PAGE_FRAME) == as->as_stackpbase);

	vbase1 = as->as_vbase1;
	vtop1 = vbase1 + as->as_npages1 * PAGE_SIZE;
	vbase2 = as->as_vbase2;
	vtop2 = vbase2 + as->as_npages2 * PAGE_SIZE;
	stackbase = USERSTACK - DUMBVM_STACKPAGES * PAGE_SIZE;
	stacktop = USERSTACK;

	if (faultaddress >= vbase1 && faultaddress < vtop1) {
		paddr = (faultaddress - vbase1) + as->as_pbase1;
	}
	else if (faultaddress >= vbase2 && faultaddress < vtop2) {
		paddr = (faultaddress - vbase2) + as->as_pbase2;
	}
	else if (faultaddress >= stackbase && faultaddress < stacktop) {
		paddr = (faultaddress - stackbase) + as->as_stackpbase;
	}
	else {
		return EFAULT;
	}

	/* make sure it's page-aligned */
	KASSERT((paddr & PAGE_FRAME) == paddr);

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_read(&ehi, &elo, i);
		if (elo & TLBLO_VALID) {
			continue;
		}
		ehi = faultaddress;
		elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
		DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr);
		tlb_write(ehi, elo, i);
		splx(spl);
		return 0;
	}

	kprintf("dumbvm: Ran out of TLB entries - cannot handle page fault\n");
	splx(spl);
	return EFAULT;
}
#endif
struct addrspace *
as_create(void)
{

	struct addrspace *as = kmalloc(sizeof(struct addrspace));
	int spl = splhigh();
	//kprintf("AS_CREATE: %x\n", (int)as);
	if (as==NULL) {
		return NULL;
	}

	as->as_vbase1 = 0;
	as->as_pbase1 = 0;
	as->as_npages1 = 0;
	as->as_vbase2 = 0;
	as->as_pbase2 = 0;
	as->as_npages2 = 0;
	as->as_stackpbase = 0;
	#if OPT_A3
	as->mode = 0;
	as->pagetable = NULL;
	#endif
	splx(spl);
	return as;
}

void
as_destroy(struct addrspace *as)
{
	#if OPT_A3

	int spl = splhigh();
	//kprintf("AS_DESTROY: %x\n", (int)as);
	splx(spl);
	if (as != NULL) {
		spl = splhigh();
		//kprintf("PT_DESTROY: %x\n", (int)as->pagetable);
		splx(spl);
		//kprintf("wtf: %x\n", (int)as->pagetable);
		kfree(as->pagetable);
		spl = splhigh();
		//kprintf("PT_DESTROYED: %x\n", (int)as->pagetable);
		splx(spl);
	}
	//void* pt = kmalloc(5 * PAGE_SIZE);
	//kprintf("wtf: %x", (int)pt);
	//kfree(pt);
	#endif
	kfree(as);
		spl = splhigh();
	//kprintf("AS_DESTROYED: %x\n", (int)as);
		splx(spl);
	//vm_cmprint();
}

void
as_activate(void)
{
	int i, spl;
	struct addrspace *as;

	as = curproc_getas();
#ifdef UW
        /* Kernel threads don't have an address spaces to activate */
#endif
	if (as == NULL) {
		return;
	}

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void
as_deactivate(void)
{
	/* nothing */
}
#if OPT_A3
int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
		 int readable, int writeable, int executable)
{
	size_t npages; 

	/* Align the region. First, the base... */
	sz += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;

	/* ...and now the length. */
	sz = (sz + PAGE_SIZE - 1) & PAGE_FRAME;

	npages = sz / PAGE_SIZE;

	/* We don't use these - all pages are read-write */
	(void)readable;
	(void)writeable;
	(void)executable;

	if (as->as_vbase1 == 0) {
		as->as_vbase1 = vaddr;
		as->as_npages1 = npages;
		return 0;
	}

	if (as->as_vbase2 == 0) {
		as->as_vbase2 = vaddr;
		as->as_npages2 = npages;
		return 0;
	}

	/*
	 * Support for more than two regions is not available.
	 */
	kprintf("dumbvm: Warning: too many regions\n");
	return EUNIMP;
}
#else
int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
		 int readable, int writeable, int executable)
{
	size_t npages; 

	/* Align the region. First, the base... */
	sz += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;

	/* ...and now the length. */
	sz = (sz + PAGE_SIZE - 1) & PAGE_FRAME;

	npages = sz / PAGE_SIZE;

	/* We don't use these - all pages are read-write */
	(void)readable;
	(void)writeable;
	(void)executable;

	if (as->as_vbase1 == 0) {
		as->as_vbase1 = vaddr;
		as->as_npages1 = npages;
		return 0;
	}

	if (as->as_vbase2 == 0) {
		as->as_vbase2 = vaddr;
		as->as_npages2 = npages;
		return 0;
	}

	/*
	 * Support for more than two regions is not available.
	 */
	kprintf("dumbvm: Warning: too many regions\n");
	return EUNIMP;
}
#endif

static
void
as_zero_region(paddr_t paddr, unsigned npages)
{
	(void) paddr;
	(void) npages;
	//bzero((void *)PADDR_TO_KVADDR(paddr), npages * PAGE_SIZE);
}

int
as_prepare_load(struct addrspace *as)
{
	KASSERT(as->as_pbase1 == 0);
	KASSERT(as->as_pbase2 == 0);
	KASSERT(as->as_stackpbase == 0);

	
	#if OPT_A3
	//int spl = 0;

	//kprintf("test %d", ((int)as->as_npages1 + (int)as->as_npages2 + 12));
	as->pagetable = kmalloc(sizeof(struct pageentry)*((int)as->as_npages1 + (int)as->as_npages2 + 12));
	int spl = splhigh();
	//kprintf("PT_CREATE: %x\n", (int)as->pagetable);
	//kprintf("PAGETABLE ADDR: %x, %x, SIZE: %d\n", (int)&(as->pagetable), (int)as->pagetable, (sizeof(struct pageentry)*((int)as->as_npages1 + (int)as->as_npages2 + 12)));
	for (int i = 0; i < (int)as->as_npages1; i++) {
		as->pagetable[i].vaddr = as->as_vbase1 + i * PAGE_SIZE;
		as->pagetable[i].paddr = getppages(1);
		//kprintf("x%x\n", as->pagetable[i].paddr);
		if (i == 0) {
			//kprintf("%d", i);
			as->as_pbase1 = as->pagetable[i].paddr;
			if (as->as_pbase1 == 0) {
				kprintf("Out of memory.\n");
				sys__exit(-1);
			}
			if (as->as_vbase1 == 0) {
				kprintf("Out of VM\n");
				sys__exit(-1);
			}
		}
		as->pagetable[i].v = 1;
		as->pagetable[i].r = 1;
		as->pagetable[i].w = 0;
		as->pagetable[i].x = 1;
		//kprintf("TEXT %d\n", i);
		//DEBUG(DB_THREADS, "Page entry text index %d: PADDR: %x VADDR: %x\n", i, as->pagetable[i].paddr, as->pagetable[i].vaddr);
	}
	splx(spl);
	//vm_cmprint();
	#else
	as->as_pbase1 = getppages(as->as_npages1);
	if (as->as_pbase1 == 0) {
		return ENOMEM;
	}
	#endif

	
	#if OPT_A3
	//struct pageentry *newpageentry; 
		//kprintf("NOW DATA %d, %d\n", (int)as->as_npages1, ((int)as->as_npages1 + (int)as->as_npages2));
	spl = splhigh();
	for (int i = (int)as->as_npages1; i < ((int)as->as_npages1 + (int)as->as_npages2); i++) {
		as->pagetable[i].vaddr = as->as_vbase2 + i * PAGE_SIZE;
		as->pagetable[i].paddr = getppages(1);
		//kprintf("%d\n", i);
		if (i == (int)as->as_npages1) {
			//kprintf("%d", i);
			as->as_pbase2 = as->pagetable[i].paddr;
			if (as->as_pbase2 == 0) {
				kprintf("Out of memory.\n");
				sys__exit(-1);
			}
			if (as->as_vbase2 == 0) {
				kprintf("Out of VM\n");
				sys__exit(-1);
			}
		}
		as->pagetable[i].v = 1;
		as->pagetable[i].r = 1;
		as->pagetable[i].w = 1;
		as->pagetable[i].x = 1;
		//kprintf("DATA %d\n", i);
		//DEBUG(DB_THREADS, "Page entry data index %d: PADDR: %x VADDR: %x\n", i, as->pagetable[i].paddr, as->pagetable[i].vaddr);

		//kprintf("5");
	}
	splx(spl);
	//kprintf("5\n");
	#else 
	as->as_pbase2 = getppages(as->as_npages2);
	if (as->as_pbase2 == 0) {
		return ENOMEM;
	}
	#endif

	#if OPT_A3
	//struct pageentry *newpageentry; 
	spl = splhigh();
	for (int i = (int)as->as_npages1 +(int)as->as_npages2; i < (int)as->as_npages1 + (int)as->as_npages2 + DUMBVM_STACKPAGES; i++) {
		as->pagetable[i].vaddr = MIPS_KSEG0 - i * PAGE_SIZE;
		as->pagetable[i].paddr = getppages(1);
		if (i == (int)as->as_npages1 +(int)as->as_npages2) {
			//kprintf("%d", i);
			as->as_stackpbase = as->pagetable[i].paddr;
			if (as->as_stackpbase == 0) {
				kprintf("Out of memory.\n");
				sys__exit(-1);
			}
		}
		as->pagetable[i].v = 1;
		as->pagetable[i].r = 1;
		as->pagetable[i].w = 1;
		as->pagetable[i].x = 1;
		//DEBUG(DB_THREADS, "Page entry stack index %d: PADDR: %x VADDR: %x\n", i, as->pagetable[i].paddr, as->pagetable[i].vaddr);
	}
	splx(spl);
	/*DEBUG(DB_THREADS, "Pages: %d, %d, %d\n",(int)as->as_npages1,(int)as->as_npages2, 12);
	for (int i = 0; i < (int)as->as_npages1 + (int)as->as_npages2 + 12; i++) {
		DEBUG(DB_THREADS, "Page entry index %d: PADDR: %x VADDR: %x\n", i, as->pagetable[i].paddr, as->pagetable[i].vaddr);
	}*/
	//vm_cmprint();
	
	#else

	as->as_stackpbase = getppages(DUMBVM_STACKPAGES);
	if (as->as_stackpbase == 0) {
		return ENOMEM;
	}
	#endif

	#if OPT_A3
	//int spl = 0;
	spl = splhigh();
	int ptaddr = (int)as;

	//kprintf("PAGETABLEADDR: %x\n", (int)(KVADDR_TO_PADDR(ptaddr)));
	for (int i = 0; i < (int)as->as_npages1 + (int)as->as_npages2 + 12; i++) {
		coremap[(as->pagetable[i].paddr/PAGE_SIZE)].index = KVADDR_TO_PADDR(ptaddr);
	}
	splx(spl);
	//vm_cmprint();

	//vm_cmprint();
	#endif
	as_zero_region(as->as_pbase1, as->as_npages1);
	//as_zero_region(as->as_pbase2, as->as_npages2);
	//as_zero_region(as->as_stackpbase, DUMBVM_STACKPAGES);
	
	return 0;
}

int
as_complete_load(struct addrspace *as)
{
	(void)as;
	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	KASSERT(as->as_stackpbase != 0);

	*stackptr = USERSTACK;
	return 0;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *new;
	//vm_cmprint();
	//int spl = splhigh();
	new = as_create();
	if (new==NULL) {
		return ENOMEM;
	}

	new->as_vbase1 = old->as_vbase1;
	new->as_npages1 = old->as_npages1;
	new->as_vbase2 = old->as_vbase2;
	new->as_npages2 = old->as_npages2;

	/* (Mis)use as_prepare_load to allocate some physical memory. */
	if (as_prepare_load(new)) {
		as_destroy(new);
		return ENOMEM;
	}

	KASSERT(new->as_pbase1 != 0);
	KASSERT(new->as_pbase2 != 0);
	KASSERT(new->as_stackpbase != 0);

	memmove((void *)PADDR_TO_KVADDR(new->as_pbase1),
		(const void *)PADDR_TO_KVADDR(old->as_pbase1),
		old->as_npages1*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_pbase2),
		(const void *)PADDR_TO_KVADDR(old->as_pbase2),
		old->as_npages2*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_stackpbase),
		(const void *)PADDR_TO_KVADDR(old->as_stackpbase),
		DUMBVM_STACKPAGES*PAGE_SIZE);
	
	*ret = new;
	//splx(spl);
	//vm_cmprint();
	return 0;
}
